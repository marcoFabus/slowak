#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Identification of slow-waves in single-channel or multi-channel 
electrophysiological data.

Routines:
    find_swa
    find_sw_1d
    find_sw_mv
    
@author: MSFabus

"""


import matplotlib.pyplot as plt
import scipy.io
import numpy as np
import emd, mne
import os
from scipy.signal import savgol_filter, stft
import it_emd
import pandas as pd

# Helper functions

def neg_dur(x):
    #Negative duration of cycle
    neg_durn = np.sum(x<0)
    return neg_durn

def trough_time(x, t):
    #Finds timing of trough of cycle
    ix_trough = np.argmin(x)
    trough_t = t[ix_trough]
    return trough_t

def p2p(x):
    #Peak to peak amplitude function
    return abs(np.amax(x)-np.amin(x))

def degree_nonlinearity(x):
    """Compute degree of nonlinearity. Eqn 3 in
    https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0168108"""
    y = np.sum(((x-x.mean()) / x.mean())**2)
    return np.sqrt(y / len(x))


def find_swa(data, sample_rate, freq=[0.49, 1.51], method='dB'):
    
    f, t, Z = stft(data, sample_rate, nperseg=4*sample_rate, noverlap=3*sample_rate, nfft=4*sample_rate)
    SWA_f = np.logical_and(f>freq[0], f<freq[1])
    ps = np.abs(Z)
    SWA_uV = np.mean(ps[SWA_f, :], 0)
    
    if method == 'dB':
        SWA = 10*np.log(SWA_uV)
        SWA_smooth = savgol_filter(SWA, 51, 3)
        
    if method == 'uV':
        SWA = SWA_uV
        SWA_smooth = savgol_filter(SWA, 51, 3)
        
    return [SWA, SWA_smooth]


def find_sw_1d(data, t, segments, sample_rate, fxconc=None, modes=[1, 2, 3],
               conditions = ['neg_dur>0.125'], pre_loaded=False,
               save_imfs=False, **kwargs):
    
    N_m = len(modes)
    
    if pre_loaded:
        imf = data
    else: 
        imf, mask_avg, mask_std, _ = it_emd.it_emd_seg(data, t, segments, 
                                                   sample_rate, N_imf=6)

    if save_imfs:
        f_path = kwargs['path']
        np.save(f_path, imf)
        
    IP, IF, IA = emd.spectra.frequency_transform(imf, sample_rate, 'nht')
    
    dfs = [[] for _ in list(range(N_m))]
    
    for i in range(N_m):
        m = modes[i]
        C = emd.cycles.Cycles(IP[:, m])
        C.compute_cycle_metric('start', t, func=np.min)
        C.compute_cycle_metric('freq', IF[:, m], func=np.mean)
        C.compute_cycle_metric('p2p', imf[:, m], func=p2p)
        C.compute_cycle_metric('p2p_eeg', data, func=p2p)
        C.compute_cycle_metric('neg_amp', imf[:, m], func=np.min)
        C.compute_cycle_metric('neg_dur', imf[:, m], func=lambda x: neg_dur(x)/sample_rate)
        C.compute_cycle_metric('trough_time', (imf[:, m], t), func=trough_time)
        C.compute_cycle_metric('DoN', IF[:, m], func=degree_nonlinearity)
        
        if fxconc is not None:
            C.compute_cycle_metric('fxconc', fxconc, func=np.min)
            
        dfs[i] = C.get_metric_dataframe(conditions=conditions)
        
    return dfs


def align_sw(cycles, conditions=None, ref_chs=None, ch_pos=None):
    # Function to identify slow waves across scalp
    # Cycles = list of channels, each a list of modes with pandas df of cycles
    # Conditions only get applied after alignment 
    # Condition format: [variable, [N_modes x variable min]]
    # Tunable parameters:   ref_chs, conditions, min active channels (3),
    #                       min time betwe sw (1s)
    
    N_m = len(cycles[0])
    N_ch = len(cycles)
    dfs = [[] for _ in list(range(N_m))]
    
    if ref_chs is None:
        ref_chs = [16, 17, 18, 4, 5, 10, 11, 14, 15, 19]
            
    for m in range(N_m):
        print(m, end=' ')

        tts = np.zeros((10000, N_ch))*np.nan     #Trough times
        params = np.zeros((10000, N_ch, 7))*np.nan

        row_list = []
        starts = []
                     
        for ch in range(N_ch):
            tt = np.array(cycles[ch][m]['trough_time'])
            params[:len(tt), ch, 0] = np.array(cycles[ch][m]['p2p'])
            params[:len(tt), ch, 1] = np.array(cycles[ch][m]['p2p_eeg'])
            params[:len(tt), ch, 2] = np.array(cycles[ch][m]['neg_amp'])
            params[:len(tt), ch, 3] = np.array(cycles[ch][m]['neg_dur'])
            params[:len(tt), ch, 4] = np.array(cycles[ch][m]['freq'])
            params[:len(tt), ch, 5] = np.array(cycles[ch][m]['DoN'])
            params[:len(tt), ch, 6] = np.array(cycles[ch][m]['fxconc'])
            tts[:len(tt), ch] = tt
            
        N_c = np.max(np.sum(~np.isnan(tts[:, ref_chs]), axis=0))
                     
        # Detect cycles within 0.2s of a reference channel, save one with max globality
        for c in range(N_c):
            det_sw = np.zeros(len(ref_chs))
            for r in range(len(ref_chs)):
                if np.isnan(tts[c, r]): continue
                delays = tts - tts[c, r]
                num_sw = np.sum(np.abs(delays) < 0.2)
                det_sw[r] = num_sw
            best_ch = ref_chs[np.argmax(det_sw)]
            delays_best = tts - tts[c, best_ch]
            active = np.abs(delays_best) < 0.2
            active_chs, active_chs_sw_ix = np.where(active.T == True)
            
            if len(active_chs) > 3:
                delays_ref = delays_best[active_chs_sw_ix, active_chs]
                origin = active_chs[np.argmin(delays_ref)]
                origin_sw_ix = active_chs_sw_ix[np.argmin(delays_ref)]
                t0 = tts[origin_sw_ix, origin]
                
                # If we have not seen this wave before, save it
                if not np.any(np.abs(starts-t0) < 0.375):
                    starts.append(t0)
                    delays = (tts-tts[origin_sw_ix, origin])[active_chs_sw_ix, active_chs]
                    globality = len(active_chs) / N_ch
                    
                    p2p = params[active_chs_sw_ix, active_chs, 0]
                    p2p_eeg = params[active_chs_sw_ix, active_chs, 1]
                    neg_amp = params[active_chs_sw_ix, active_chs, 2]
                    neg_dur = params[active_chs_sw_ix, active_chs, 3]
                    freqs = params[active_chs_sw_ix, active_chs, 4]
                    DoN = params[active_chs_sw_ix, active_chs, 5]
                    
                    fxconc = params[origin_sw_ix, origin, 6]
                    
                    # Speed estimation
                    max_delay = np.max(delays)
                    last_chan = active_chs[np.argmax(delays)]
                    dist_vec = ch_pos[origin] - ch_pos[last_chan]
                    dist = np.linalg.norm(dist_vec) * 0.14 # m, assumes spherical D = 0.35m head model
                    speed = dist / max_delay
                    
                    
                    dic = {'start':t0, 'fxconc':fxconc, 'chs':active_chs, 'delays':delays,
                           'p2p':p2p, 'p2p_eeg':p2p_eeg, 'neg_amp':neg_amp,
                           'neg_dur':neg_dur, 'freqs':freqs, 'DoN':DoN,
                           'origin':origin, 'speed':speed, 'globality':globality}
                    row_list.append(dic)
            
            # Convert to pd data frame
            df = pd.DataFrame(row_list)
            
            if conditions is not None:
                for cond in len(conditions):
                    var = conditions[cond][0]
                    min_val = conditions[cond][1][m]
                    cond_true = df[var] > min_val
                    df = df[cond_true]
                
            dfs[m] = df
            
    return dfs