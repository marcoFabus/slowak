#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Tools for analysis of waves detected by slowak.detection

Routines:
    wave_slice
    topogan
    
@author: MSFabus

"""

import matplotlib.pyplot as plt
import scipy.io
import numpy as np
import emd, mne
import os
from scipy.signal import savgol_filter, stft
from scipy.optimize import curve_fit
import it_emd
from statsmodels.sandbox.stats.multicomp import multipletests
from scipy import stats
from collections import deque
from bisect import insort, bisect_left
from itertools import islice
import dcor


def cycle_slice(cycles, dur_cond=0.125, amp_cond=50, amp_cond_mode='rel'):
    
    N_imfs = cycles.shape[0]
    out = [[] for _ in range(N_imfs)]
    
    for j in range(N_imfs):
        
        if amp_cond_mode == 'rel':
            amp_lim = np.nanpercentile(cycles[j, :, 5], amp_cond)
            print(amp_lim)
            
        if amp_cond_mode == 'abs':
             amp_lim = amp_cond
        
        mask = np.logical_and(cycles[j, :, 3] > dur_cond, 
                              cycles[j, :, 5] > amp_lim)
        
        cyc = cycles[j, :, :].copy()
        cyc[~mask] = np.nan
        
        nan_waves = np.any(np.isnan(cyc), axis=1)
        out[j] = np.delete(cyc, nan_waves, axis=0)
        
    return out

    
def corr_ttest(x1, x2, verbose=False, method='two-sided', alpha=0.05, p_lim=0.05):
    
    N = x1.shape[1]
    pvalue = np.zeros(N)*np.nan
    t = np.zeros(N)*np.nan
    H0_rej = np.zeros(N)*np.nan
    
    for i in range(N):
        t[i], pvalue[i] = stats.ttest_ind(x1[:,i], x2[:,i], nan_policy='omit',
                                        equal_var=False)
        
    if method == 'one-sided':
        pvalue[t<0] = 1
        alpha /= 2

    reject, corrpvalue, alphacSidak, alphacBonf = multipletests(pvalue, alpha=alpha, method='bonferroni')
    
    H0_rej[corrpvalue >= p_lim] = np.nan
    H0_rej[corrpvalue < p_lim] = 1
    
    if verbose:
        return [pvalue, corrpvalue, H0_rej]
    else:
        return H0_rej
    
    
def running_filt(x, window_size, mode='mean'):
    
    from scipy.ndimage.filters import uniform_filter1d
    if mode == 'mean':
        res = uniform_filter1d(x, size=window_size)
        return res
    
    if mode == 'median':
        x= iter(x); d = deque(); s = []; result = []
        for item in islice(x, window_size):
            d.append(item)
            insort(s, item)
            result.append(s[len(d)//2])
        m = window_size // 2
        for item in x:
            old = d.popleft()
            d.append(item)
            del s[bisect_left(s, old)]
            insort(s, item)
            result.append(s[m])
        return result


def PMSI(imf, m, method='both'):
    
    if method == 'both':
        abs1 = (imf[:, m].dot(imf[:, m]) + imf[:, m-1].dot(imf[:, m-1]))
        pmsi1 = np.max([np.dot(imf[:, m], imf[:,m-1]) / abs1, 0])
        abs2 = (imf[:, m].dot(imf[:, m]) + imf[:, m+1].dot(imf[:, m+1]))
        pmsi2 = np.max([np.dot(imf[:, m], imf[:,m+1]) / abs2, 0])
        return pmsi1 + pmsi2
    
    if method == 'above':
        abs1 = (imf[:, m].dot(imf[:, m]) + imf[:, m-1].dot(imf[:, m-1]))
        pmsi1 = np.max([np.dot(imf[:, m], imf[:,m-1]) / abs1, 0])
        return pmsi1
    
    if method == 'below':
        abs2 = (imf[:, m].dot(imf[:, m]) + imf[:, m+1].dot(imf[:, m+1]))
        pmsi2 = np.max([np.dot(imf[:, m], imf[:,m+1]) / abs2, 0])
        return pmsi2


def shape_recon(imf, m, sample_rate, th=0.1, mask=None,
                verbose=False, **kwargs):
    
    IP, IF, IA = emd.spectra.frequency_transform(imf, sample_rate, 'hilbert',
                                             smooth_phase=5)
    x = imf[:, m]
    y = imf[:, m-1]
    ip1 = IP[:, m]
    ip2 = IP[:, m-1]
    wf = np.zeros(48)
    wf_std = np.zeros(48)
    
    f = np.mean(IF[:, m]) / np.mean(IF[:, m-1])
    a = np.mean(IA[:, m]) / np.mean(IA[:, m-1])
    
    dcorr = dcor.distance_correlation(ip1, ip2)
    p, _ = dcor.independence.distance_correlation_t_test(ip1, ip2)
    
    flag = 0
    if dcorr > th and p<0.05:
        if a >= 1.05 and a*f >=1:
            flag = 1
        if a < 1.05 and a*f < 1:
            flag = 1
            
    if flag == 0:
        print('IMF recon not recommended')
        if mask is None:
            mask = np.ones(imf.shape[0], dtype=bool)
        if mask == 'f_th':
            mask = IF[:, m] < kwargs['f_th']
            
        cycles = emd.cycles.get_cycle_inds(IP[:,m], return_good=True, mask=mask)
        pa = emd.cycles.phase_align(IP[:,m], IF[:,m], cycles=cycles)
        wf = pa.mean(axis=1)
        wf_std = pa.std(axis=1)
        
        if verbose:
            N = cycles.max()
            return wf, wf_std, dcorr, N, x, a, a*f
        
        return wf, wf_std
        
    imf_r = x + y
    imf_r = np.reshape(imf_r, (len(imf_r), 1))

    IPr, IFr, IAr = emd.spectra.frequency_transform(imf_r, sample_rate, 'hilbert',
                                                 smooth_phase=5)
    if mask is None:
        mask = np.ones(imf.shape[0], dtype=bool)
    if mask == 'f_th':
        mask = IFr[:, 0] < kwargs['f_th']
    if mask == 'amp_th':
        mask =IAr[:, 0] < np.percentile(IAr[:, 0],  kwargs['amp_th'])
        
    cycles = emd.cycles.get_cycle_inds(IPr[:,0], return_good=True, mask=mask)
    pa = emd.cycles.phase_align(IPr[:,0], IFr[:,0], cycles=cycles)
    wf = np.mean(pa, axis=1)
    wf_std = pa.std(axis=1)
    
    if verbose:
        N = cycles.max()
        return wf, wf_std, dcorr, N, imf_r, a, a*f
    
    return wf, wf_std


def fit_swa_model(swa, fxconc, p0=None):
    
    if p0 is None:
        p0 = [np.percentile(swa, .05), np.percentile(swa, .95) - np.percentile(swa, .05),
             np.mean(swa),  0.6]
        
    def f(x, a, b, c, d):
        return a + b / (1 + np.exp(-(x-c)/d))
        
        
    popt, pcov = curve_fit(f, fxconc, swa, p0=p0)
    a = popt[0]; b = popt[1]; c = popt[2]; d = popt[3]
    Pswas = a + b
    Cswas = c - d * np.log(1/0.95 - 1)              # Assumes SWAS when SWA = a + 0.95b, i.e. step matters
    Cswas = c - d * np.log(b/(0.95*(a+b)-a) - 1)    # Assumes SWAS when SWA = 0.95(a+b), i.e. absolute level matters
    fitted = f(fxconc, a, b, c, d)
    
    res = {'Pswas':Pswas, 'Cswas':Cswas, 'fitted':fitted, 'param':popt,
           'cov':pcov}
    
    return res