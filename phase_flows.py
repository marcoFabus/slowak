#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 09:49:09 2021

Tools for phase flow analysis of slow waves.

@author: MSFabus
"""

import numpy as np
import emd, mne
from scipy import ndimage
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import utils
import imageio, os
from pygifsicle import optimize
from scipy.signal import savgol_filter
import yt
from yt.visualization.api import Streamlines
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler


def smooth_phase_spatial(data, chs, raw, modes, sample_rate, input_mode='imf',
                         res=15, kernel_size=1, out_wrapped=True, 
                         smooth_wrapped=False):
    # Smooths unwrapped phase spatially across scalp
    N_m = len(modes)
    N_ch = len(chs)
    
    if input_mode == 'imf':
        N_s = data.shape[2]
        IP_all = np.zeros((N_ch, N_m, N_s))
        for ch in range(N_ch):
            print(ch)
            IP, _, _ = emd.spectra.frequency_transform(data[ch, :, :].T, sample_rate, 'nht', 
                                                       wrapped=False)
            IP_all[ch, :, :] = IP[:, modes].T
    
    if input_mode == 'IP':
        N_s = data.shape[0]
        IP_all = data
    
    if smooth_wrapped:
        IP_all = IP_all % (2 * np.pi)
        
    IP_all_smooth = np.zeros((N_m, N_s, res, res))
    for m in range(N_m):
        for i in range(N_s):
            if i % 10000 == 0 and i > 0:
                print(i)
            
            f, ax = plt.subplots()
            X, Y, Z, pos, outlines = mne.viz.plot_topomap(IP_all[:, m, i], raw.info, sphere=1, show=False,
                                           res=res, ret_Z=True, axes=ax)
            f.clear()
            plt.close(f)
            
            if kernel_size == 0:
                Z_smooth = Z
            else:
                Z_smooth = ndimage.gaussian_filter(Z, kernel_size, mode='nearest')
                
            IP_all_smooth[m, i, :, :] = Z_smooth
            
    if out_wrapped:
        out = (IP_all_smooth) % (2 * np.pi)
    else:
        out = IP_all_smooth
        
    return out, pos, outlines, X, Y


def velocity_field(IP, sample_rate, nas_in=0.35):
    
    res = IP.shape[-1]
    grad = np.gradient(IP, 0.8*nas_in/res, edge_order=1, axis=[2, 3])
    dphi_dt = np.abs(np.gradient(IP, 1/sample_rate, edge_order=1, axis=[1]))
    gX = grad[1]; gY = grad[0]
    u = -1 * dphi_dt * gX / (gX**2 + gY**2)
    v = -1 * dphi_dt * gY / (gX**2 + gY**2)
    speed = np.sqrt(u**2 + v**2)
    
    return u, v, speed


def streamlines(u, v, num_t=2, ends_only=True):

    w = v*0
    res = u.shape[-1]
    res_t = u.shape[0]
    
    data = dict(velocity_x=(u, "m/s"), velocity_y=(v, "m/s"), 
                velocity_z=(w, "m/s"))
    bbox = np.array([[-1, 1], [-1, 1], [-1, 1]])
    ds = yt.load_uniform_grid(data, [res_t, res, res], bbox=bbox, nprocs=64)
    scale = ds.domain_width[1]
    
    pos_dx = np.mgrid[0.0001:0.9999:num_t*1j, 0.0001:0.9999:res*1j, 0.0001:0.9999:res*1j].reshape(3,-1).T

    pos_dx = pos_dx * scale - scale/2
    pos = pos_dx
    
    sink_streamlines = Streamlines(ds, pos, 'velocity_z',  'velocity_y', 'velocity_x', get_magnitude=False,
                              length=10, direction=1)
    sink_streamlines.integrate_through_volume()
    sink_sls = sink_streamlines.streamlines
    
    source_streamlines = Streamlines(ds, pos, 'velocity_z',  'velocity_y', 'velocity_x', get_magnitude=False,
                              length=10, direction=-1)
    source_streamlines.integrate_through_volume()
    source_sls = source_streamlines.streamlines
    
    if ends_only:
        source_sls = source_sls[:, -5:, :] #TODO: what if there is just one long streamline and this fails?
        source_sls = source_sls[source_sls[:, 0, 2] != 0, :, :]
        sink_sls = sink_sls[:, -5:, :]
        sink_sls = sink_sls[sink_sls[:, 0, 2] != 0, :, :]
    
    return sink_sls, source_sls


def centroid(pts):
    
    avg = np.mean(pts, axis=0)
    
    return avg


def cluster_stream_ends(stream_pts, num_t, ret_centroid=True, eps=0.2, min_n=10):
    
    # Input N_streamlines x Npts x [T, Y, X]
    ts = np.unique(stream_pts[:, :, 0])
    out = [[] for _ in list(range(num_t))]
    i = 0
    for t in ts:
        X = stream_pts[stream_pts[:, :, 0] == t][:, 1:]
    
        db = DBSCAN(eps=eps, min_samples=min_n).fit(X)
        core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
        core_samples_mask[db.core_sample_indices_] = True
        labels = db.labels_
        
        unique_labels = set(labels).remove(-1) if -1 in labels else set(labels)
        
        if unique_labels is None:
            continue
        if len(unique_labels) == 0:
            continue
        
        if ret_centroid:
            out[i] = [centroid(X[(labels == k) & core_samples_mask])
                      for k in unique_labels]
        else:
            out[i] = [X[(labels == k) & core_samples_mask] for k in unique_labels]
        
        i += 1
        
    return out


def classify_singularities(u, v, num_t=20, ret_centroid=True, input_mode='speed'):
    
    if input_mode == 'speed':
        # 0 = sink, 1 = source. Output: [N_t x [x, y, type]]
        sink_sl, source_sl = streamlines(u, v, num_t)
        sinks = cluster_stream_ends(sink_sl, num_t, ret_centroid)
        sources = cluster_stream_ends(source_sl, num_t, ret_centroid)
        out = [[] for _ in list(range(num_t))]
    
    if input_mode == 'singularities':
        sinks = u; sources = v
        num_t = len(sinks)
        out = [[] for _ in list(range(num_t))]
    
    
    for i in range(num_t):
        if len(sinks[i]) > 0:
            sink_pts = np.vstack(sinks[i])
        else:
            continue
        if len(sources[i]) > 0:
            source_pts = np.vstack(sources[i])
        else:
            continue
        
        sink_pts = np.append(sink_pts, np.zeros((sink_pts.shape[0], 1)), 1)
        source_pts = np.append(source_pts, np.ones((source_pts.shape[0], 1)), 1)
        
        all_pts = np.concatenate((sink_pts, source_pts))
        
        out[i] = all_pts
    
    return out
        
            

def phase_synchrony(IP):
    """
    Calculates coherence order parameter.

    Parameters
    ----------
    IP : N_s x res x res ndarray.

    Returns
    -------
    R : 

    """
    resX = IP.shape[-1]
    resY = IP.shape[-2]
    N_s = IP.shape[0]
    S = resX * resY
    R = np.zeros(N_s)
    IP_flat = np.reshape(IP, (N_s, resX*resY))
    
    for i in range(N_s):
        r = 1 / S * np.abs(np.sum(np.exp(1j * IP_flat[i, :])))
        R[i] = r
        
    return R


def avg_norm_vel(u, v, smooth=False, window=None):    
    su = np.sum(u, axis=(-2, -1))
    sv = np.sum(v, axis=(-2, -1))
    num = np.sqrt(su**2 + sv**2).flatten()
    
    abs_w = np.sqrt(u**2 + v**2)
    den = np.sum(abs_w, axis=(-2, -1)).flatten()
    
    phi = num / den
    
    if smooth:
        phi_smooth = savgol_filter(phi, window, 3)
        return phi, phi_smooth
    
    
    return phi


def topoplot(data, X, Y, outlines, r0=1, streamlines=True, V=None, st_pts=None,
             classify=True, ax=None, interpolation=None, show=True,
             show_sources=True, vmin=0, vmax=2*np.pi):



        im = ax.imshow(data, vmin=vmin, vmax=vmax,
                        extent=[-1, 1, -1, 1], cmap='RdBu_r', origin='lower',
                    aspect='equal', interpolation=interpolation)

        plt.colorbar(im)
            
        patch = patches.Circle((0, 0), radius=r0, transform=ax.transData) 
        im.set_clip_path(patch)
        
        if streamlines:
            u = V[0]; v = V[1]; speed = V[2]
            lw = 1# speed / np.nanmean(speed)
            sc = ax.streamplot(X[0, :], Y[:,0], u, v, density=1, color='k', linewidth=lw,
                               arrowsize=0.5)
            
            sc.lines.set_clip_path(patch)
            for arrow_patch in plt.gca().patches[-len(sc.arrows.get_paths()):]:
                arrow_patch.set_clip_path(patch)
        
        if show_sources:
            
            if len(st_pts) > 0:
                cols = ['r', 'g', 'b', 'c', 'k']
                labs = ['sink', 'source', 'spiral in', 'spiral out', 'saddle']
                x = st_pts[:, 1]
                y = st_pts[:, 0]
                
                if classify:
                    ptt = np.array(st_pts[:, 2], dtype=int) #stationary point types
                    
                for i in range(len(x)):
                    if classify:
                        ax.plot(x[i], y[i], marker='o', color=cols[ptt[i]], 
                                label=labs[ptt[i]], markersize=1)
                    else:
                        ax.plot(x[i], y[i], marker='o', color='darkorange',
                                markersize=15)
                
                if classify:
                    utils.legend_wo_duplicates(ax, loc=3, ncol=1, prop={'size': 6})
        
        mne.viz.topomap._draw_outlines(ax, outlines)
        plt.axis('off')
        
        if show:
            plt.show()
        
        return
    

def animate_flow(IP, X, Y, t, outlines, V=None, st_pts=None,
                 base_name='./anim/', durn=0.15, save_all=False,
                 optimize_gif=True, **kwargs):
    
    N_s = IP.shape[0]
    u = V[0]; v = V[1]; speed = V[2]
    fnames = []
    
    if not os.path.exists(base_name):
        os.makedirs(base_name)
    
    for s in range(N_s):
        t0 = t[s]
        IPc = IP[s, :, :]
        u0 = u[s, :, :]
        v0 = v[s, :, :]
        sp = speed[s, :, :]
        pts = st_pts[s]
        
        fig, ax = plt.subplots()
        topoplot(IPc, X, Y, outlines, V=[u0, v0, sp], 
                 st_pts=pts, ax=ax, show=False, **kwargs)
        ax.set_title('T=%.3f'%t0 + 's')
        
        fname = base_name + '_' + str(s) + '.png'
        fig.savefig(fname, bbox_inches='tight')
        fnames.append(fname)
        plt.close()
     
    gif_name = base_name + '_anim.gif'
    with imageio.get_writer(gif_name, mode='I', duration=durn) as writer:
        for filename in fnames:
            image = imageio.imread(filename)
            writer.append_data(image)
    
    if optimize_gif:
        optimize(gif_name) 
    
    if save_all:
        return
    
    for filename in set(fnames):
        os.remove(filename)
    
    return
        