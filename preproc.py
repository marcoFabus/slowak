#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Pre-processing tools for slow-wave analysis.

Routines:
    butter_bandpass
    butter_bandpass_filter
    reject_art_simple
    resample
    plot_psd
    
@author: MSFabus

"""

import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import butter, sosfiltfilt, welch
from scipy import interpolate

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    sos = butter(order, [low, high], btype='band', output='sos')
    return sos


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    sos = butter_bandpass(lowcut, highcut, fs, order=order)
    y = sosfiltfilt(sos, data)
    return y


def reject_art_simple(data, n_std=5, n_sec=1, sample_rate=250):
    cond = n_std * np.std(data)
    arg_bad = np.argwhere(np.abs(data) > cond).flatten()
    mask = np.unique(np.array([list(range(x - n_sec * sample_rate, 
                                          x + n_sec * sample_rate)) 
                               for x in arg_bad]).flatten())
    data_proc = np.delete(data, mask)
    return data_proc


def resample(x, y, interp_x):
    #Resample fn along interp_ph for a given (IP, fn) pair
    i_fn = interpolate.interp1d(x, y, fill_value="extrapolate")
    interp_y = i_fn(interp_x)
    return interp_y


def plot_psd(data, sample_rate, nperseg=None, return_psd=False, xlim=None,
             ylim=None):
    
    if nperseg is None:
        nperseg = 4 * sample_rate
    freqs, psd = welch(data, sample_rate, nperseg=nperseg)
    plt.rc('font', size=32)
    fig, ax = plt.subplots(figsize=(12,8))
    ax.semilogy(freqs,psd, color='k', lw=2)
    
    if xlim is None:
        ax.set_xlim([0.5, 20])
    else:
        ax.set_xlim(xlim)
        
    if ylim is not None:
        ax.set_ylim(ylim)
    else:
        ax.set_ylim([psd.min()*0.9, psd.max() * 1.1])
    ax.grid(True)
    ax.set_xlabel('Freq [Hz]')
    ax.set_ylabel('Spectral Density')
    
    if return_psd:
        plt.close(fig=fig)
        return [freqs, psd]
    return ax