#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 14:22:59 2021

@author: MSFabus
"""

import sys
sys.path.insert(0, "/home/marco/Insync/sjoh4485@ox.ac.uk/OneDrive Biz/PhD Year 1/EMD/Analysis Code/slowak")

import preproc
import detection
import analysis
import sim
import it_emd
import phase_flows
import utils