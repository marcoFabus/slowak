#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Extraction of IMFs using iterated masking EMD (itEMD).

Routines:
    it_emd
    plot_imf
    example_sim
    
@author: MSFabus

"""

import matplotlib.pyplot as plt
import numpy as np
import emd, mne
from scipy import interpolate, optimize, ndimage
from analysis import PMSI
import warnings
warnings.simplefilter('always', UserWarning)


def it_emd(x, sample_rate=512, mask_0='zc', N_imf=6, 
           N_iter_max=15, iter_th=0.1, N_avg=1, exclude_edges=False, 
           verbose=False, plot_opt=False, w_method='power', 
           plot_all=False, ignore_last=False, **kwargs):
    """
    Runs iterated masking EMD. mask_0 is in normalised units, i.e.
    divided by sample rate.
    
    Runs until N_iter_max or until converged + N_avg iterations.
    """

    sample_rate = sample_rate
    data = x

        
    #N_avg += 1; N_iter_max += 1
    samples = len(data)
    
    if mask_0 == 'nyquist':
        mask = np.array([sample_rate/2**(n+1) for n in range(1, N_imf+1)])/sample_rate
    elif mask_0 =='zc':
        _, mask = emd.sift.mask_sift(x, max_imfs=N_imf, mask_freqs='zc',
                                       ret_mask_freq=True)
    elif mask_0 == 'random':
        mask = np.random.randint(0, sample_rate/4, size=N_imf) / sample_rate
    else:
        mask = mask_0
    
    mask_all = np.zeros((N_iter_max+N_avg, N_imf))
    imf_all = np.zeros((N_iter_max+N_avg, N_imf, samples))
    
    niters = 0; niters_c = 0
    navg = 0
    maxiter_flag = 0
    continue_iter = True
    converged = False
    
    while continue_iter:
        
        if not converged:
            print(niters)
        else:
            print('Converged, averaging... ' + str(niters_c) + ' / ' + str(N_avg))
        
        mask_all[niters+niters_c, :len(mask)] = mask
        imf = emd.sift.mask_sift(data, max_imfs=N_imf, mask_freqs=mask,
                                 mask_amp_mode='ratio_imf')
        IP,IF,IA = emd.spectra.frequency_transform(imf, sample_rate, 'nht')
        mask_prev = mask
        
        if exclude_edges:
            ex = int(0.025*samples)
            samples_included = list(range(ex, samples-ex)) #Edge effects ignored
        else:
            samples_included = list(range(samples)) #All
        
        if w_method == 'IA':
            IF_weighted = np.average(IF[samples_included, :], 0, weights=IA[samples_included, :])
        if w_method == 'power':
            IF_weighted = np.average(IF[samples_included, :], 0, weights=IA[samples_included, :]**2)
        if w_method == 'avg':
            IF_weighted = np.mean(IF[samples_included, :], axis=0)
            
        mask = IF_weighted/sample_rate
        imf_all[niters+niters_c, :imf.shape[1], :] = imf.T

        l = min(len(mask), len(mask_prev))
        
        if ignore_last:
            l -= 1
        mask_variance = np.abs((mask[:l] - mask_prev[:l]) / mask_prev[:l]) 
        
        if np.all(mask_variance[~np.isnan(mask_variance)] < iter_th) or converged: 
            converged = True
            if navg < N_avg:
                navg += 1
            else:
                continue_iter = False
        
        if not converged:
            niters += 1
        else:
            niters_c += 1
        
        if plot_all:
            #print(np.round(mask*sample_rate, 4))
            #print(np.round(mask_variance, 4))
            Nimf = np.sum(~np.isnan(mask))
            N_plot = 6 if Nimf>6 else Nimf
            plot_imf(imf[:, :N_plot], secs=[14, 16], sample_rate=sample_rate) 
            plt.show()
        
        if niters >= N_iter_max:
            warnings.warn('Maximum number of iterations reached')
            maxiter_flag = 1
            continue_iter = False
        
    print('N_iter = ', niters)
        
    imf_final = np.nanmean(imf_all[niters:niters+N_avg, :, :], axis=0).T
    IF_final = np.nanmean(mask_all[niters:niters+N_avg, :], axis=0)*sample_rate
    IF_std_final = np.nanstd(mask_all[niters:niters+N_avg, :], axis=0)*sample_rate
    
    if N_avg == 1:
        IF_std_final = mask_variance
    
    N_imf_final = int(np.sum(~np.isnan(mask_all[niters-1, :])))
    imf_final = imf_final[:, :N_imf_final]
    IF_final = IF_final[:N_imf_final]
    IF_std_final = IF_std_final[:N_imf_final]
    
    imf = imf_final
    
    if plot_opt:
        plt.figure(figsize=(12,6))
        plt.rc('font', size=16) 
        for j in range(N_imf):
            plt.plot(sample_rate*mask_all[:, j], label='IMF-{0}'.format(j+1), 
                     linewidth=2)#np.mean(IA[:, j])/np.max(np.mean(IA[:, j]) * 20))
        plt.grid(True)
        plt.ylabel('IF [Hz]')
        plt.xlabel('Iteration')
        plt.legend()
        plt.xlim([0, niters-1])
        plt.show()
    
    if verbose:
        return [niters, mask_all, imf_final, IF_final, IF_std_final, imf_all]
    
    return [imf_final, IF_final, IF_std_final, niters, maxiter_flag]


def it_emd_seg(data, t, segments, sample_rate, N_imf=8, joint=True, **kwargs):
        
    print('\n Processing segment:')
    out = [[] for _ in range(len(segments)-1)]
    imf_all = np.zeros((len(data), N_imf))*np.nan
    mask_all = np.zeros((len(segments)-1, N_imf))*np.nan
    niters_all = np.zeros(len(segments)-1)*np.nan
    ctr = 0
    
    for s in range(len(segments)-1):
        
        print('\n %s / %s' %(s+1, len(segments)-1))
             
        #Select slice
        ROI = np.logical_and(t > segments[s], t < segments[s+1])
        x = data[ROI]
        time_vect = t[ROI]
           
        [imf, mask_eq, mask_var, niters, _] = it_emd(x, sample_rate=sample_rate, **kwargs)
        
        out[s] = [imf, mask_eq, mask_var, time_vect, niters]
        
        N = imf.shape[1]
        imf_all[ctr:ctr+len(x), :N] = imf
        mask_all[s, :N] = mask_eq
        niters_all[s] = niters
        
        ctr += len(x)
        
    if joint:
        mask_avg = np.nanmean(mask_all, axis=0)
        mask_std = np.nanstd(mask_all, axis=0)
        N_imf_nz = np.sum(~np.isnan(mask_eq))
        imf_all = imf_all[:, :N_imf_nz]
        keep = ~np.isnan(imf_all).all(axis=1)
        imf_all = imf_all[keep, :]

        return [imf_all, mask_avg, mask_std, niters_all]
        
    else:
        return out


def plot_imf(imf, secs=[0, 2], sample_rate=256, figsize=(12,8), **kwargs):
        
    idx = list(range(int(secs[0]*sample_rate), int(secs[1]*sample_rate)))
    t = np.linspace(0, secs[1]-secs[0], len(idx))
    plt.rc('font', size=18) 
    fg = plt.figure(figsize=figsize)
    emd.plotting.plot_imfs(imf[idx, :], cmap=True, scale_y=True, fig=fg, **kwargs, time_vect=t)
    return fg


