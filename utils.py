#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 09:47:12 2021

@author: MSFabus
"""

import numpy as np
from scipy import ndimage
from collections import deque
from bisect import insort, bisect_left
from itertools import islice
from scipy.ndimage.filters import uniform_filter1d
import warnings

def im_extrema(data, mode='max', dist=5, th=1):
    if mode == 'max':
        data_ex = ndimage.filters.maximum_filter(data, dist)
    if mode == 'min':
        data_ex = ndimage.filters.minimum_filter(data, dist)
    extrema = (data == data_ex)
    diff = np.abs(data_ex) > th
    extrema[diff == 0] = 0
    labeled, num_objects = ndimage.label(extrema)
    slices = ndimage.find_objects(labeled)
    x, y = [], []
    for dy,dx in slices:
        x_center = (dx.start + dx.stop) / 2
        x.append(x_center)
        y_center = (dy.start + dy.stop) / 2
        y.append(y_center)
    return np.array(x), np.array(y)


def jacobian(u, v, nas_in=0.35):
    res = u.shape[-1]
    Du = np.gradient(u, 0.8*nas_in/res, edge_order=1, axis=[2, 3])
    Dv = np.gradient(v, 0.8*nas_in/res, edge_order=1, axis=[2, 3])
    du_dx = Du[1]; du_dy = Du[0]; dv_dx = Dv[1]; dv_dy = Dv[0]
    det = du_dx*dv_dy - dv_dx*du_dy
    trace = du_dx + dv_dy
    return det, trace


def legend_wo_duplicates(ax, **kwargs):
    handles, labels = ax.get_legend_handles_labels()
    unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
    ax.legend(*zip(*unique), **kwargs)
    

def running_median(seq, window_size):
    """Contributed by Peter Otten"""
    seq = iter(seq); d = deque(); s = []; result = []
    for item in islice(seq, window_size):
        d.append(item)
        insort(s, item)
        result.append(s[len(d)//2])
    m = window_size // 2
    for item in seq:
        old = d.popleft()
        d.append(item)
        del s[bisect_left(s, old)]
        insort(s, item)
        result.append(s[m])
    return result

def running_mean(seq, window_size):
    return uniform_filter1d(seq, size=window_size)

def hod_hist(arr1, arr2, edges, func):
    # Higher-order histogram - split array 1 into chunks based on 
    # edges from array 2 and apply func
    # Array 1 and 2 are 1D and same length
    out = np.zeros(len(edges)-1) * np.nan
    N = np.zeros(len(edges)-1) * np.nan
    
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        for i in range(len(edges)-1):
            ix = np.where((arr2 > edges[i]) & (arr2 < edges[i+1]))
            out[i] = func(arr1[ix])
            N[i] = len(ix[0])
    centres = (edges[1:] + edges[:-1])/2
    return out, centres, N