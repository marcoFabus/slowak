![plot](./slowak_logo.png)

A python package for analysis of electrophysiological slow-waves build on Empirical Mode Decomposition (emd). 

```
import slowak as sw
```

## Modules:

### sw.preproc     

Tools for pre-processing of signal.

### sw.it_emd     

Iterated EMD (itEMD) functionality. Build on emd toolbox (see https://emd.readthedocs.io)

### sw.sim         

Tools for simulating a signal.

### sw.detection   

Tools for detecting slow waves.

### sw.analysis    

Tools for analysis of slow wave object from sw.detection.
